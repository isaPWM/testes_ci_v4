import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {
    @Test
    void soma(){
        Calculatrice c = new Calculatrice();
        c.soma(1, 3);
        assertEquals(" 4", c.getResultado());
    }
    @Test
    void subtracao(){
        Calculatrice c = new Calculatrice();
        c.subtracao(3, 1);
        assertEquals(" 2", c.getResultado());
    }
    @Test
    void multiplicacao(){
        Calculatrice c = new Calculatrice();
        c.multiplicacao(9, 9);
        assertEquals(" 81", c.getResultado());
        c.multiplicacao(2147483647, 2147483647);
        assertEquals("overflow", c.getResultado());
    }
}