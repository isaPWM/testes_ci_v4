public class Calculatrice {
    private String resultado;
    public Calculatrice(){
    }
    public void soma(int a, int b){
        resultado = " " + (a + b);
    }
    public void subtracao(int a, int b){
        resultado = " " + (a - b);
    }
    public void multiplicacao(Integer a, Integer b){
        if(a == null || b == null){
            resultado = "input(s) invalido(s)";
            return;
        }
        if(a < Integer.MAX_VALUE && b < Integer.MAX_VALUE) {
            System.out.println(a * b);
            resultado = " " + (a * b);
        }else
            resultado = "overflow";
    }
    public String getResultado(){
        return resultado;
    }
}
